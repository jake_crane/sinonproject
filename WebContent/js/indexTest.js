QUnit.test( "test index.add", function( assert ) {
	 assert.equal(index.add(1, 1), 2, "1 + 1 = 2");
});

QUnit.test( "spy testing", function( assert ) {
	var spy = sinon.spy(index.innerIndex, "multiply");
	index.innerIndex.multiply(2, 3);
	assert.equal(spy.args[0][0], 2, "first arg was correct");
	assert.equal(spy.args[0][1], 3, "second arg was correct");
	assert.ok(spy.calledWithExactly(2, 3), "spy was called with correct arguments");
	assert.ok(spy.withArgs(2, 3).calledOnce, "function was called once with correct agrs");
	assert.equal(spy.getCall(0).returnValue, 6, "spy returned correct value");
	spy.restore();
});
 
QUnit.test( "test index.add with stub", function( assert ) {
	var stub = sinon.stub(index, "add", function() {return 1;});
	 assert.equal(index.add(1, 1), 1, "stub returned 1");
	 stub.restore();
	 assert.equal(index.add(1, 1), 2, "stub returned 2");
});

QUnit.test( "more stub testing", function( assert ) {
	var stub = sinon.stub(index.innerIndex, "multiply", function() {return 1;});
	 assert.equal(index.innerIndex.multiply(2, 2), 1, "stub returned 1");
	 stub.restore();
	 assert.equal(index.innerIndex.multiply(2, 2), 4, "stub returned 4");
});

QUnit.test( "mock testing", function( assert ) {
	var mock = sinon.mock(index.innerIndex);
    mock.expects("multiply").returns(4).once();
    index.innerIndex.multiply(2, 2);
    mock.verify();
    mock.restore();
});
